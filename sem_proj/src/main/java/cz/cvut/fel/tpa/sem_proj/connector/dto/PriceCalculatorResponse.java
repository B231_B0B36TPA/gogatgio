package cz.cvut.fel.tpa.sem_proj.connector.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PriceCalculatorResponse {
    private int translationPrice;
}
