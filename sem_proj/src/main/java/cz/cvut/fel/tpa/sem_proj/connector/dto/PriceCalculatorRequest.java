package cz.cvut.fel.tpa.sem_proj.connector.dto;

import lombok.Data;

@Data
public class PriceCalculatorRequest {
    private int textLength;
    private TranslationType translationType;
    private Boolean isFormal;
}
