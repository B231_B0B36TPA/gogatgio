package cz.cvut.fel.tpa.sem_proj.worker;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotificationServiceWorker {

    @JobWorker(type = "notify-customer-about-cancellation-service-task")
    public void notifyCustomerAboutCancellation(final ActivatedJob job) {
        // Send automatic sms.

        log.info("Customer notified about cancellation.");
    }

    @JobWorker(type = "notify-customer-about-needing-payment-service-task")
    public void notifyCustomerAboutNeedingPayment(final ActivatedJob job) {
        // Send automatic sms.

        log.info("Customer notified about needing payment.");
    }
}
