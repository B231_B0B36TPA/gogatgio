package cz.cvut.fel.tpa.sem_proj.connector.dto;

public enum TranslationType {
    AUTOMATIC,
    MANUAL
}
