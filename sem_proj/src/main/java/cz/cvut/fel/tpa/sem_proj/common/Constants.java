package cz.cvut.fel.tpa.sem_proj.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {
    public static final String CUSTOMER_ID = "customerId";
    public static final String CUSTOMER_PHONE_NUMBER = "customerPhoneNumber";
    public static final String ACCOUNT_BALANCE = "accountBalance";
    public static final String TRANSLATION_PRICE = "translationPrice";
}
