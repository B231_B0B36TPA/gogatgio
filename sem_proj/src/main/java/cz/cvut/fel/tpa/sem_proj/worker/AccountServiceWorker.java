package cz.cvut.fel.tpa.sem_proj.worker;

import cz.cvut.fel.tpa.sem_proj.common.Constants;
import cz.cvut.fel.tpa.sem_proj.repository.Account;
import cz.cvut.fel.tpa.sem_proj.repository.AccountRepository;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class AccountServiceWorker {
    private final AccountRepository accountRepository;


    @JobWorker(type = "load-account-data-service-task")
    public Map<String, Object> loadAccountData(final ActivatedJob job) {
        var variables = job.getVariablesAsMap();
        var customerId = (Integer) variables.get(Constants.CUSTOMER_ID);

        var account = accountRepository.getAccount(customerId);

        var accountBalance = Optional.ofNullable(account)
                .map(Account::getAccountBalance)
                .orElse(0);
        var customerPhoneNumber = Optional.ofNullable(account)
                .map(Account::getCustomerPhoneNumber)
                .orElse("Customer phone number missing!");

        variables.put(Constants.ACCOUNT_BALANCE, accountBalance);
        variables.put(Constants.CUSTOMER_PHONE_NUMBER, customerPhoneNumber);

        log.info("Loaded accountBalance {} and customerPhoneNumber {} for customerId {}",
                accountBalance, customerPhoneNumber, customerId
        );

        return variables;
    }

    @JobWorker(type = "charge-customer-service-task")
    public void chargeCustomerAccount(final  ActivatedJob job) {
        var variables = job.getVariablesAsMap();
        var customerId = (Integer) variables.get(Constants.CUSTOMER_ID);
        var translationPrice = (Integer) variables.get(Constants.TRANSLATION_PRICE);

        var account = accountRepository.getAccount(customerId);
        var accountBalance = account.getAccountBalance();

        account.setAccountBalance(accountBalance - translationPrice);

        log.info("Customer with id {} was charged {} Kč. New account balance is {}", customerId, translationPrice, account.getAccountBalance());
    }
}
