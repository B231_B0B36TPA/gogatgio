package cz.cvut.fel.tpa.sem_proj.repository;

import org.springframework.stereotype.Repository;

import java.util.Map;


@Repository
public class AccountRepository {
    /**
     * The key is the customerId.
     */
    private final Map<Integer, Account> accounts = Map.of(
            1, new Account(1, "+420234678123", 0),
            2, new Account(2, "123456789", 1099),
            3, new Account(3, "00420987654321", 999999)
    );

    public Account getAccount(Integer customerId) {
        return accounts.get(customerId);
    }
}
