package cz.cvut.fel.tpa.sem_proj;

import io.camunda.zeebe.spring.client.annotation.Deployment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// I am deploying manually, because linked forms can't be deployed
// and new connector versions wouldn't be used.
//@Deployment(resources = "classpath*:/bpmn/**/*.bpmn")
@SpringBootApplication
public class SemProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(SemProjApplication.class, args);
    }

}
