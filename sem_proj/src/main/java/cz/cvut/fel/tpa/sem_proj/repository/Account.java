package cz.cvut.fel.tpa.sem_proj.repository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Account {
    private final Integer customerId;
    private String customerPhoneNumber;
    private Integer accountBalance;
}
