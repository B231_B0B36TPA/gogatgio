package cz.cvut.fel.tpa.sem_proj.controller;

import cz.cvut.fel.tpa.sem_proj.common.Constants;
import io.camunda.zeebe.client.ZeebeClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
public class TranslationProcessController {
    private final ZeebeClient zeebeClient;

    private static final String BPMN_PROCESS_ID = "Process_Translation_GG";

    @PostMapping("/process/translation")
    public void startNotificationProcess(@RequestParam int customerId) {
        log.info("Starting translation process for customer {}", customerId);
        zeebeClient.newCreateInstanceCommand()
                .bpmnProcessId(BPMN_PROCESS_ID)
                .latestVersion()
                .variable(Constants.CUSTOMER_ID, customerId)
                .send();
    }
}
