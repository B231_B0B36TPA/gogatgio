package cz.cvut.fel.tpa.sem_proj.worker;

import cz.cvut.fel.tpa.sem_proj.common.Constants;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class PaymentServiceWorker {
    @JobWorker(type = "verify-payment-service-task")
    public void verifyPayment(final  ActivatedJob job) {
        var variables = job.getVariablesAsMap();
        var customerId = (Integer) variables.get(Constants.CUSTOMER_ID);
        var translationPrice = (Integer) variables.get(Constants.TRANSLATION_PRICE);

        log.info("Verified payment for customer with id {}. Amount {} Kč.", customerId, translationPrice);
    }
}
