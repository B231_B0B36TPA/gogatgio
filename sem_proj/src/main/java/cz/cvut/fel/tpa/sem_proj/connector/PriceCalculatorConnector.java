package cz.cvut.fel.tpa.sem_proj.connector;

import cz.cvut.fel.tpa.sem_proj.connector.dto.PriceCalculatorRequest;
import cz.cvut.fel.tpa.sem_proj.connector.dto.PriceCalculatorResponse;
import cz.cvut.fel.tpa.sem_proj.connector.dto.TranslationType;
import io.camunda.connector.api.annotation.OutboundConnector;
import io.camunda.connector.api.outbound.OutboundConnectorContext;
import io.camunda.connector.api.outbound.OutboundConnectorFunction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@OutboundConnector(
        name = "Price calculator connector",
        inputVariables = {"textLength", "translationType", "isFormal"},
        type = "cz.cvut.fel.tpa.sem_proj.connector.calculate-price:1"
)
public class PriceCalculatorConnector implements OutboundConnectorFunction {

    private static final Double PRICE_PER_CHAR = 1.0;
    private static final Double AUTOMATIC_TRANSLATION_PRICE_MULTIPLIER = 0.001;
    private static final Double MANUAL_TRANSLATION_PRICE_MULTIPLIER = 1.5;

    private static final Double FORMAL_MULTIPLIER = 1.25;

    @Override
    public Object execute(OutboundConnectorContext context) {
        final var connectorRequest = context.bindVariables(PriceCalculatorRequest.class);

        var textLength = connectorRequest.getTextLength();
        var translationType = connectorRequest.getTranslationType();
        var isFormal = connectorRequest.getIsFormal();

        var price = calculatePrice(textLength, translationType, isFormal);

        log.info("Calculated price %s Kč for textLength %s, translationType %s and isFormal %s."
                .formatted(price, textLength, translationType, isFormal)
        );

        return new PriceCalculatorResponse(price);
    }

    private Integer calculatePrice(int textLength, TranslationType translationType, boolean isFormal) {
        var translationPriceMultiplier = translationType == TranslationType.AUTOMATIC
                ? AUTOMATIC_TRANSLATION_PRICE_MULTIPLIER
                : MANUAL_TRANSLATION_PRICE_MULTIPLIER;

        if (isFormal) {
            translationPriceMultiplier *= FORMAL_MULTIPLIER;
        }

        // TODO delete this after testing
        translationPriceMultiplier *= 1000;

        return (int) Math.round(textLength * PRICE_PER_CHAR * translationPriceMultiplier);
    }
}
