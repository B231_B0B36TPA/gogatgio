package cz.cvut.fel.tpa.pata_uloha.connector.dto;

public enum TranslationType {
    AUTOMATIC,
    MANUAL
}
