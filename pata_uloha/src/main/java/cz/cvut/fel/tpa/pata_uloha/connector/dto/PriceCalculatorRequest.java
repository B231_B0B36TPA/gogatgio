package cz.cvut.fel.tpa.pata_uloha.connector.dto;

import lombok.Data;

@Data
public class PriceCalculatorRequest {
    private Integer textLength;
    private TranslationType translationType;
}
