package cz.cvut.fel.tpa.pata_uloha.connector;

import cz.cvut.fel.tpa.pata_uloha.connector.dto.PriceCalculatorRequest;
import cz.cvut.fel.tpa.pata_uloha.connector.dto.PriceCalculatorResponse;
import cz.cvut.fel.tpa.pata_uloha.connector.dto.TranslationType;
import io.camunda.connector.api.annotation.OutboundConnector;
import io.camunda.connector.api.outbound.OutboundConnectorContext;
import io.camunda.connector.api.outbound.OutboundConnectorFunction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@OutboundConnector(
        name = "Price calculator connector",
        inputVariables = {"textLength", "translationType"},
        type = "cz.cvut.fel.czm.tpa.calculate-price:1"
)
public class PriceCalculatorConnector implements OutboundConnectorFunction {

    private static final Double PRICE_PER_CHAR = 1.0;
    private static final Double AUTOMATIC_TRANSLATION_PRICE_MULTIPLIER = 0.001;
    private static final Double MANUAL_TRANSLATION_PRICE_MULTIPLIER = 1.5;


    @Override
    public Object execute(OutboundConnectorContext context) {
        final var connectorRequest = context.bindVariables(PriceCalculatorRequest.class);

        var textLength = connectorRequest.getTextLength();
        var translationType = connectorRequest.getTranslationType();

        var price = calculatePrice(textLength, translationType);

        log.info("Calculated price %s Kč for textLength %s and translationType %s."
                .formatted(price, textLength, translationType)
        );

        return new PriceCalculatorResponse(price);
    }

    private Integer calculatePrice(Integer textLength, TranslationType translationType) {
        var translationPriceMultiplier = translationType == TranslationType.AUTOMATIC
                ? AUTOMATIC_TRANSLATION_PRICE_MULTIPLIER
                : MANUAL_TRANSLATION_PRICE_MULTIPLIER;

        return (int) Math.round(textLength * PRICE_PER_CHAR * translationPriceMultiplier);
    }
}
