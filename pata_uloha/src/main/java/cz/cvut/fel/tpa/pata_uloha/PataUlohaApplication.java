package cz.cvut.fel.tpa.pata_uloha;

import io.camunda.zeebe.spring.client.annotation.Deployment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Deployment(resources = "classpath*:/bpmn/**/*.bpmn")
@SpringBootApplication
public class PataUlohaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PataUlohaApplication.class, args);
    }

}
