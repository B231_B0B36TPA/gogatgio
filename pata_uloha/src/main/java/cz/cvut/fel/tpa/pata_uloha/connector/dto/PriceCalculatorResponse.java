package cz.cvut.fel.tpa.pata_uloha.connector.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class PriceCalculatorResponse {
    private Integer translationPrice;
}
