package cz.cvut.fel.tpa.treti_uloha;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.process.test.api.ZeebeTestEngine;
import io.camunda.zeebe.spring.test.ZeebeSpringTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static io.camunda.zeebe.process.test.assertions.BpmnAssert.assertThat;
import static io.camunda.zeebe.protocol.Protocol.USER_TASK_JOB_TYPE;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@SpringBootTest
@ZeebeSpringTest
class NotificationTests {

	@Autowired
	private ZeebeClient zeebeClient;
	@Autowired
	private ZeebeTestEngine zeebeTestEngine;

	@Test
	void testCustomerNotification() throws InterruptedException, TimeoutException {
		// Start new process instance
		var processInstance = zeebeClient.newCreateInstanceCommand()
				.bpmnProcessId("customer-notification-process")
				.latestVersion()
				.send()
				.join();

		// Check if process started
		assertNotNull(processInstance.getBpmnProcessId());

		// Let the workflow engine do whatever it needs to do
		zeebeTestEngine.waitForIdleState(Duration.ofMinutes(5));

		// Get user tasks
		List<ActivatedJob> userTasks = zeebeClient
						.newActivateJobsCommand()
						.jobType(USER_TASK_JOB_TYPE)
						.maxJobsToActivate(1)
						.workerName("waitForUserTaskCallCustomer")
						.send()
						.join()
						.getJobs();

		// There should be 0 or 1 user tasks
		assertTrue(userTasks.size() <= 1, "Too many user tasks found.");

		// If there is a user task, then complete it.
		if (!userTasks.isEmpty()) {
			zeebeClient
					.newCompleteCommand(userTasks.get(0))
					.send()
					.join();
		}

		// Let the workflow engine do whatever it needs to do
		zeebeTestEngine.waitForIdleState(Duration.ofMinutes(5));

		// Check if process completed
		assertThat(processInstance)
				.isCompleted();
	}

}
