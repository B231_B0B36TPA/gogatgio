package cz.cvut.fel.tpa.treti_uloha;

import io.camunda.zeebe.spring.client.annotation.Deployment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@Deployment(resources = "classpath*:/bpmn/**/*.bpmn")
@SpringBootApplication
public class TretiUlohaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TretiUlohaApplication.class, args);
	}

}
