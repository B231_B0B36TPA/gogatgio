package cz.cvut.fel.tpa.treti_uloha.controller;

import io.camunda.zeebe.client.ZeebeClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
public class NotificationController {
    private final ZeebeClient zeebeClient;

    @PostMapping("/process/notify-customer")
    public void startNotificationProcess() {
        log.info("Starting customer notification process");
        zeebeClient.newCreateInstanceCommand()
                .bpmnProcessId("customer-notification-process")
                .latestVersion()
                .send();
    }

}
