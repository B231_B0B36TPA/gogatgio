package cz.cvut.fel.tpa.treti_uloha.worker;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import io.camunda.zeebe.spring.client.exception.ZeebeBpmnError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class NotificationServiceWorker {
    private final Random random = new Random();

    @JobWorker(type = "notify-customer-service-task")
    public void notifyCustomer(final ActivatedJob job) {
        if (random.nextBoolean()) {
            log.info("Customer notified");
        } else {
            var msg = "Automatic user notification failed";
            log.error(msg);
            throw new ZeebeBpmnError("AUTOMATIC_CUSTOMER_NOTIFICATION_FAIL", msg);
        }
    }
}
