package tpa.fel.cvut.cz.druha_uloha.common;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class Constants {
    public static final String TRANSLATION_PAYMENT_PROCESS_ID = "Process_TranslationPayment";
    public static final String GET_TRANSLATION_COST_TASK_TYPE = "getTranslationCost";
    public static final String CHARGE_TRANSLATION_COST_TASK_TYPE = "chargeTranslationCost";
    public static final String TEXT_2_TRANSLATE = "text2Translate";
    public static final String ACCOUNT_BALANCE = "accountBalance";
    public static final String TRANSLATION_COST = "translationCost";
    public static final String IS_SUCCESSFUL = "isSuccessful";
}
