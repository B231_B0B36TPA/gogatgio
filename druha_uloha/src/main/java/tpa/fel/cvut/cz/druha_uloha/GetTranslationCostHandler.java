package tpa.fel.cvut.cz.druha_uloha;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.client.api.worker.JobClient;
import io.camunda.zeebe.client.api.worker.JobHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

import static tpa.fel.cvut.cz.druha_uloha.common.Constants.TEXT_2_TRANSLATE;
import static tpa.fel.cvut.cz.druha_uloha.common.Constants.TRANSLATION_COST;

@Component
@RequiredArgsConstructor
public class GetTranslationCostHandler implements JobHandler {
    private static final int MAX_FREE_TRANSLATION_TEXT_LENGTH = 20;
    private static final int PRICE_PER_ADDITIONAL_CHAR = 1;

    @Override
    public void handle(JobClient client, ActivatedJob job) throws Exception {
        final Map<String, Object> inputVariables = job.getVariablesAsMap();
        final String text2Translate = (String) inputVariables.get(TEXT_2_TRANSLATE);

        var translationCost = Double.valueOf(
                Math.max(
                        (text2Translate.trim().length() - MAX_FREE_TRANSLATION_TEXT_LENGTH) * PRICE_PER_ADDITIONAL_CHAR,
                        0
                )
        );

        client.newCompleteCommand(job.getKey())
                .variables(Map.of(TRANSLATION_COST, translationCost))
                .send()
                .join();
    }
}
