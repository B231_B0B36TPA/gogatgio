package tpa.fel.cvut.cz.druha_uloha;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.impl.oauth.OAuthCredentialsProvider;
import io.camunda.zeebe.client.impl.oauth.OAuthCredentialsProviderBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Duration;
import java.util.Map;

import static tpa.fel.cvut.cz.druha_uloha.common.Constants.*;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class TranslationPaymentApplication implements CommandLineRunner {

    private final GetTranslationCostHandler getTranslationCostHandler;
    private final ChargeTranslationCostHandler chargeTranslationCostHandler;

    @Value("${zeebe.address}")
    private String zeebeAddress;
    @Value("${zeebe.client_id}")
    private String zeebeClientId;
    @Value("${zeebe.client_secret}")
    private String zeebeClientSecret;
    @Value("${zeebe.authorization_server_url}")
    private String zeebeAuthorizationServerUrl;
    @Value("${zeebe.token_audience}")
    private String zeebeTokenAudience;

    public static void main(String[] args) {
        SpringApplication.run(TranslationPaymentApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        final OAuthCredentialsProvider credentialsProvider =
                new OAuthCredentialsProviderBuilder()
                        .authorizationServerUrl(zeebeAuthorizationServerUrl)
                        .audience(zeebeTokenAudience)
                        .clientId(zeebeClientId)
                        .clientSecret(zeebeClientSecret)
                        .build();

        try (final ZeebeClient client =
                     ZeebeClient.newClientBuilder()
                             .gatewayAddress(zeebeAddress)
                             .credentialsProvider(credentialsProvider)
                             .build()) {
            final Map<String, Object> variables = Map.of(
                    TEXT_2_TRANSLATE, "Tohle mi přelož, protože to potřebuju na předmět TPA.",
                    ACCOUNT_BALANCE, 200d
            );

            client.newCreateInstanceCommand()
                    .bpmnProcessId(TRANSLATION_PAYMENT_PROCESS_ID)
                    .latestVersion()
                    .variables(variables)
                    .send()
                    .join();

            client.newWorker()
                    .jobType(GET_TRANSLATION_COST_TASK_TYPE)
                    .handler(getTranslationCostHandler)
                    .timeout(Duration.ofSeconds(10).toMillis())
                    .open();

            client.newWorker()
                    .jobType(CHARGE_TRANSLATION_COST_TASK_TYPE)
                    .handler(chargeTranslationCostHandler)
                    .timeout(Duration.ofSeconds(10).toMillis())
                    .open();

            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
