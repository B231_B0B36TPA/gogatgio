package tpa.fel.cvut.cz.druha_uloha;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.client.api.worker.JobClient;
import io.camunda.zeebe.client.api.worker.JobHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

import static tpa.fel.cvut.cz.druha_uloha.common.Constants.*;

@Component
@RequiredArgsConstructor
public class ChargeTranslationCostHandler implements JobHandler {
    @Override
    public void handle(JobClient client, ActivatedJob job) throws Exception {
        final Map<String, Object> inputVariables = job.getVariablesAsMap();
        final Double translationCost = Double.valueOf((Integer)inputVariables.get(TRANSLATION_COST));
        final Double accountBalance = (Double) inputVariables.get(ACCOUNT_BALANCE);

        var isSuccessful = translationCost <= accountBalance;

        client.newCompleteCommand(job.getKey())
                .variables(Map.of(IS_SUCCESSFUL, isSuccessful))
                .send()
                .join();
    }
}
